#include <stdio.h>

int main() {
    int c;
    FILE *file;
    file = fopen("../input.txt", "r");

    if (file) {
        while ((c = getc(file)) != EOF) {
            if (c >= 'a' && c <= 'z') c -= 32;
            if (c == ' ') {
                continue;
            }
            putchar(c);
        }
        fclose(file);
    } else {
        printf("Diese Datei existiert nicht!\n");
    }
    return 0;
}